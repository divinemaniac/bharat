/**
 * Written By Bikash Paneru
 * Property of Glomeworks Tech
 */

package com.glomeworks.musicianprofile;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ContactFragment extends Fragment {
    private class ContactItem {
        private String title;
        private String text;
        private String url;
        private int drawableId;

        public ContactItem(String title, String text, String url, int drawableId) {
            this.title = title;
            this.text = text;
            this.url = url;
            this.drawableId = drawableId;
        }

        public ContactItem(String title, int textId, int urlId, int drawableId) {
            this.title = title;
            this.text = getString(textId);
            this.url = getString(urlId);
            this.drawableId = drawableId;
        }

        public String getTitle() {
            return title;
        }

        public String getText() {
            return text;
        }

        public String getUrl() {
            return url;
        }

        public int getDrawableId() {
            return drawableId;
        }
    }

    public ContactFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_contact, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final List<ContactItem> contacts = new ArrayList<>(8);
        contacts.add(new ContactItem("Email",R.string.contact_email,R.string.url_email,R.drawable.icon_gmail));
        contacts.add(new ContactItem("Facebook",R.string.contact_facebook,R.string.url_facebook,R.drawable.icon_fb));
        contacts.add(new ContactItem("Twitter",R.string.contact_twitter,R.string.url_twitter,R.drawable.icon_twitter));
        contacts.add(new ContactItem("Instagram",R.string.contact_instagram,R.string.url_instagram,R.drawable.icon_insta));
        contacts.add(new ContactItem("Youtube",R.string.full_name,R.string.url_youtube,R.drawable.icon_youtube));
        contacts.add(new ContactItem("Soundcloud",R.string.contact_soundcloud,R.string.url_soundcloud,R.drawable.icon_sc));
        contacts.add(new ContactItem("Blog",R.string.contact_blog,R.string.url_blog,R.drawable.icon_blogger));
        contacts.add(new ContactItem("Skype",R.string.contact_skype,R.string.contact_skype,R.drawable.icon_skype));

        ListView contactList = (ListView) view.findViewById(R.id.contact_list);
        contactList.setOnItemClickListener(new ListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ContactItem item = contacts.get(position);
                String url = item.getUrl();
                Intent i;
                if(url.contains("mailto:")) i = new Intent(Intent.ACTION_SENDTO);
                else i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(item.getUrl()));
                try {
                    startActivity(i);
                } catch(ActivityNotFoundException e) {}
            }
        });
        ArrayAdapter<ContactItem> adapter = new ArrayAdapter<ContactItem>(getContext(), R.layout.contact_list_item, contacts) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if(convertView == null)
                    convertView = getLayoutInflater(null).inflate(R.layout.contact_list_item, parent, false);

                ImageView icon = (ImageView) convertView.findViewById(R.id.contact_icon);
                TextView title = (TextView) convertView.findViewById(R.id.contact_title);
                TextView text = (TextView) convertView.findViewById(R.id.contact_text);

                ContactItem contact = getItem(position);

                text.setText(contact.getText());
                title.setText(contact.getTitle());
                icon.setImageResource(contact.getDrawableId());
                return convertView;
            }
        };
        contactList.setAdapter(adapter);
    }
}
