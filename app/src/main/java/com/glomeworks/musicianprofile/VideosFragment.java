/**
 * Written By Bikash Paneru
 * Property of Glomeworks Tech
 */

package com.glomeworks.musicianprofile;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.Channel;
import com.google.api.services.youtube.model.ChannelListResponse;
import com.google.api.services.youtube.model.PlaylistItem;
import com.google.api.services.youtube.model.PlaylistItemListResponse;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class VideosFragment extends Fragment implements ListView.OnItemClickListener {
    private class VideoLoader extends Thread {
        private String nextPageToken = null;

        public String getDefaultPlaylist() {
            YouTube.Channels.List query;
            try {
                query = youtube.channels().list("contentDetails");
                query.setKey(getString(R.string.youtube_data_api_key));
                query.setId(getString(R.string.youtube_channel_id));
                query.setFields("items(contentDetails/relatedPlaylists/uploads)");
                ChannelListResponse response = query.execute();
                Channel channel = response.getItems().get(0);
                return channel.getContentDetails().getRelatedPlaylists().getUploads();
            } catch (IOException e) {
                Log.d("YC","Could not resolve channel's default playlist");
                return null;
            }
        }

        public List<Video> getVideos(String playlist, @Nullable String pageToken) {
            YouTube.PlaylistItems.List query;
            try {
                query = youtube.playlistItems().list("snippet");
                query.setKey(getString(R.string.youtube_data_api_key));
                query.setPlaylistId(playlist);
                query.setMaxResults((long)50);
                //Load the page with given token, if specified
                if(pageToken != null) query.setPageToken(pageToken);
                query.setFields("nextPageToken,items(snippet/resourceId,snippet/title,snippet/thumbnails/default)");
                PlaylistItemListResponse response = query.execute();
                List<PlaylistItem> list = response.getItems();
                //Store next page token if it exists
                if(response.getNextPageToken()!= null) nextPageToken = response.getNextPageToken();
                else nextPageToken = null;
                List<Video> videos = new ArrayList<>(list.size());
                for(PlaylistItem item: list) {
                    Video video = new Video();
                    video.setId(item.getSnippet().getResourceId().getVideoId());
                    video.setTitle(item.getSnippet().getTitle());
                    video.setThumbnailUrl(item.getSnippet().getThumbnails().getDefault().getUrl());
                    videos.add(video);
                }
                return videos;
            } catch (IOException e) {
                Log.d("YC","Could not fetch videos");
                nextPageToken = null;
                return new ArrayList<Video>();
            }
        }

        public List<Video> getVideos(String playlist) {
            return getVideos(playlist, null);
        }

        public void run() {
            String playlist = getDefaultPlaylist();
            if(playlist != null) {
                List<Video> vids = getVideos(playlist);
                while(nextPageToken != null) {
                    vids.addAll(getVideos(playlist,nextPageToken));
                }

                videos = vids;

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        renderList();
                    }
                });
            }
        }
    }

    private YouTube youtube;
    private List<Video> videos;
    private Handler handler;
    private Snackbar loadingSnackBar;
    private ListView videoList;

    public VideosFragment() {
        // Required empty public constructor
    }

    /**
     * Renders the list of videos using the videos private variable
     */
    public void renderList() {
        ArrayAdapter<Video> adapter = new ArrayAdapter<Video>(getContext(), R.layout.video_list_item, videos) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if(convertView == null)
                    convertView = getLayoutInflater(null).inflate(R.layout.video_list_item, parent, false);

                ImageView thumbnail = (ImageView) convertView.findViewById(R.id.thumbnail);
                TextView title = (TextView) convertView.findViewById(R.id.title);

                Video video = getItem(position);

                title.setText(video.getTitle());
                Picasso.with(getContext()).load(video.getThumbnailUrl()).into(thumbnail);
                return convertView;
            }
        };

        videoList.setAdapter(adapter);
        loadingSnackBar.dismiss();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_videos, container, false);
        videoList = (ListView) view.findViewById(R.id.videos_list);
        videoList.setOnItemClickListener(this);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadingSnackBar = Snackbar.make(view,"Loading..",Snackbar.LENGTH_INDEFINITE);
        loadingSnackBar.show();
        new VideoLoader().start();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        youtube = new YouTube.Builder(new NetHttpTransport(),
                new JacksonFactory(),
                new HttpRequestInitializer() {
                    @Override
                    public void initialize(HttpRequest request) throws IOException {}
                }
        ).build();
        handler = new Handler();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(getContext(), PlayerActivity.class);
        intent.putExtra("VIDEO_ID", videos.get(position).getId());
        startActivity(intent);
    }

    @Override
    public void onPause() {
        super.onPause();
        if(loadingSnackBar.isShownOrQueued()) loadingSnackBar.dismiss();
    }
}
