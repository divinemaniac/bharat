/**
 * Written By Bikash Paneru
 * Property of Glomeworks Tech
 */

package com.glomeworks.musicianprofile;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.apache.http.HttpResponse;
import org.apache.http.message.BasicHttpRequest;
import org.json.JSONException;
import org.json.JSONObject;

import com.soundcloud.api.ApiWrapper;
import com.soundcloud.api.Http;
import com.soundcloud.api.Request;

import java.io.IOException;

public class MusicFragment extends Fragment {
    private class AudioLoader extends Thread {

        private JSONObject parseResponse(HttpResponse response) {
            int status = response.getStatusLine().getStatusCode();
            //Follow redirects
            if (status == 302) {
                BasicHttpRequest request = new BasicHttpRequest("GET",response.getFirstHeader("Location").getValue());
                return null;
                //request.
            } else if (status == 200) {
                try {
                    return new JSONObject(Http.formatJSON(Http.getString(response)));
                } catch(JSONException e) {
                    Log.d("SC", "Failed to resolve user id --Invalid JSON");
                    return null;
                } catch(IOException e) {
                    Log.d("SC", "Failed to resolve user id --IOException");
                    return null;
                }
            } return null;
        }

        private JSONObject get(Request request) {
            try {
                return parseResponse(soundCloud.get(request));
            } catch(IOException e) {
                Log.d("SC", "Failed to resolve user id");
                return null;
            }
        }

        private String resolveUserId(String username) {
            Request request = Request.to("/resolve")
                    .with("url", "https://soundcloud.com/" + username)
                    .with("client_id", getString(R.string.soundcloud_client_id));
            return "";
        }

        public void run() {
            Log.d("GW","entered thread");
            String userId = resolveUserId(getString(R.string.soundcloud_username));
            if(userId != null) Log.d("GW",userId);
            else Log.d("GW","null userid");
        }
    }

    public MusicFragment() {
        // Required empty public constructor
    }

    ApiWrapper soundCloud;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        soundCloud = new ApiWrapper(getString(R.string.soundcloud_client_id),"",null,null);
        new AudioLoader().start();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_music, container, false);
    }
}
