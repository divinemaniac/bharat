/**
 * Written By Bikash Paneru
 * Property of Glomeworks Tech
 */

package com.glomeworks.musicianprofile;


public class Video {
    String thumbnailUrl;
    String title;
    String id;

    public Video() {}

    public Video(String id, String title, String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
        this.title = title;
        this.id = id;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
